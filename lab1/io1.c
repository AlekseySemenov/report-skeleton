#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/version.h>

#define MAX_VALUES 40
#define STRING_LEN_PER_VALUE 24
#define STRING_MAX_LEN (MAX_VALUES * STRING_LEN_PER_VALUE)

static dev_t first;
static struct cdev c_dev; 
static struct class *cl;
static struct proc_dir_entry* entry;

static size_t vector[MAX_VALUES];
static size_t pointer;

static size_t stoa(size_t value, char* buffer) {
    size_t i = 0, j, to_return;
    do {
        buffer[i++] = value % 10 + '0';
    } while((value /= 10) > 0);
    to_return = i;
    buffer[i--] = '\0';
    for (j = 0; j < i; i--, j++) {
        char tmp = buffer[i];
        buffer[i] = buffer[j];
        buffer[j] = tmp;
    }
    return to_return;
}

static int is_letter(char c) {
    if (c >= 'A' && c <= 'Z')
        return 1;
    if (c >= 'a' && c <= 'z')
        return 1;
    return 0;
}

static int io1_open(struct inode *i, struct file *f) {
    printk(KERN_INFO "IO1: open\n");
    return 0;
}

static int io1_close(struct inode *i, struct file *f) {
    printk(KERN_INFO "IO1: close\n");
    return 0;
}

static ssize_t io1_read(struct file *f, char __user *buf, size_t len, loff_t *off) {
    size_t new_len = 0, full_len = 0, i = 0;
    char buffer[STRING_MAX_LEN];

    if (*off > 0 || len < full_len) {
        return 0;
    }
    
    for (i = 0; i < pointer; i++) {
        new_len = stoa(vector[i], buffer + full_len);
        full_len += new_len;
        buffer[full_len++] = '\n';
    }
    buffer[full_len] = '\0';

    printk(KERN_INFO "IO1: read\n%s", buffer);

    if (copy_to_user(buf, buffer, full_len)) {
        pr_err("IO1: FAIL READ");
        return -1;
    };

    *off = full_len;
    return full_len;
}

static ssize_t io1_write(struct file *f, const char __user *buf, size_t len, loff_t *off) {
    size_t i, letters;
    letters = 0;
    for (i = 0; i < len; i++)
        if (is_letter(buf[i]))
            letters++;
    
    if (pointer >= MAX_VALUES)
        return 0;
    
    vector[pointer++] = letters;
    
    printk(KERN_INFO "IO1: write\n");
    return len;
}

static int io1_uevent(struct device *dev, struct kobj_uevent_env *env) {
    add_uevent_var(env, "DEVMODE=%#o", 0666);
    return 0;
}

static struct file_operations io1_fops = {
    .owner = THIS_MODULE,
    .open = io1_open,
    .release = io1_close,
    .read = io1_read,
    .write = io1_write
};

static struct proc_ops io1_pops = {
    .proc_read = io1_read
};

static int __init io1_init(void) {
    printk(KERN_INFO "IO1: Init\n");
    if (alloc_chrdev_region(&first, 0, 1, "ch_dev") < 0) {
		return -1;
	}
    if ((cl = class_create(THIS_MODULE, "chardrv")) == NULL) {
		unregister_chrdev_region(first, 1);
		return -1;
	}

    cl->dev_uevent = io1_uevent;

    if (device_create(cl, NULL, first, NULL, "var5") == NULL) {
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	}
    cdev_init(&c_dev, &io1_fops);
    if (cdev_add(&c_dev, first, 1) == -1) {
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	}

    entry = proc_create("var5", 0444, NULL, &io1_pops);

    pointer = 0;
    return 0;
}
 
static void __exit io1_exit(void) {
    cdev_del(&c_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    proc_remove(entry);
    printk(KERN_INFO "IO1: Exit\n");
}
 
module_init(io1_init);
module_exit(io1_exit);
 
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Avdeev-Semenov");
MODULE_DESCRIPTION("IO Lab1");
